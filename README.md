# latex-poster-template
A LaTeX template to write beautiful posters.

## Writing a new poster
- fork this repository
- put all you figures in the `figures/` directory

## Compile the poster
- make sure to install the `latexmk` package and command
- run `make poster`

> :bulb: **Note**  
> the local repo can be cleaned from compilation files with `make clean`

## The poster template
![poster.pdf](poster.pdf)
